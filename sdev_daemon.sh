#!/bin/bash

#title           :/dev/rant
#description     :Not an official client for devrant.com implemented to access
#                : its API via /dev/rant/<topic> (note the similarity between 
#                : service name and path). Works in read-only mode. Updates
#                : data from API every minute. Exposes response via FIFOs
#author          :netikras
#date            :2018-07-29
#version         :1.0
#url             : https://gitlab.com/netikras/dev_rant_linux
#usage           :  sudo sdev_daemon.sh; 
#                :  cat /dev/rant/surprise
#notes           :Downloads required libraries to CWD.
#                :
#                :  Still WIP
#dependencies    :bjson.sh (JSON parser for shell): https://gitlab.com/netikras/bjson
#                : date
#                : wget
#                : curl
#===============================================================================




NL=$'\n'
CR=$'\r'
TAB=$'\t'

export IFS=" ${NL}${TAB}"
export PATH="/bin:/usr/bin"

if [ "${USER}" != "root" ] ; then
    echo "You should run server as root user";
    exit 1;
    :;
fi;


#. bjson.sh
. bjson.sh || {
    bjsonUrl="https://gitlab.com/netikras/bjson/raw/master/bjson.sh"
    echo "Downloading dependency: bjson.sh";
    wget -q "${bjsonUrl}" -O bjson.sh
    . bjson.sh || { echo "Failed" >&2; exit 1; }
}

if [ ! -d /dev/rant ] ; then
    mkdir /dev/rant;
    :;
fi;


PIDS=;

URL_DAY_FEED='https://devrant.com/api/devrant/rants?app=3&sort=recent&range=day&limit=5&skip=0'
URL_DAY_ALGO='https://devrant.com/api/devrant/rants?app=3&sort=algo&range=day&limit=5&skip=0'

URL_SURPRISE='https://devrant.com/api/devrant/rants/surprise?app=3'
URL_STORY='https://devrant.com/api/devrant/story-rants?app=3&sort=recent&range=week&limit=5&skip=0'

formatDate() {
    local timestamp=${1:-0}
    date -d "@${timestamp}" "+%Y-%m-%d %H:%M:%S"
}

buildRantText() {
    local rant=${1:-rant};
    
    local result=;
    
    local id="$(BJMODE=find parseJson "${rant}.id")"
    local text="$(BJMODE=find parseJson "${rant}.text")"
    local author="$(BJMODE=find parseJson "${rant}.user_username")"
    local user_score="$(BJMODE=find parseJson "${rant}.user_score")"
    local score="$(BJMODE=find parseJson "${rant}.score")"
    local created="$(BJMODE=find parseJson "${rant}.created_time")"
    local image="$(BJMODE=find parseJson "${rant}.attached_image.url")"
    local image_w="$(BJMODE=find parseJson "${rant}.attached_image.width")"
    local image_h="$(BJMODE=find parseJson "${rant}.attached_image.height")"
    local tagids="$(BJMODE=find parseJson "${rant}.tags")"
#    echo "All tagids: ${tagids}, dummy tag: $(BJMODE=find parseJson "${rant}.tags[0]")" >&2
    tagids=${tagids/,/}
    local tags;

    for tagid in ${tagids//,/ } ; do
#       echo "Pulling tagid ${tagid}" >&2
        tags="${tags}, $(BJMODE=find parseJson "${rant}.tags[${tagid}]")";
    done;
    
    result="${result}${NL}";
    result="${result}rant ${id} ++${score}     by ${author} ++${user_score}, $(formatDate "${created}")${NL}${NL}"
    result="${result}${text}${NL}${NL}"
    if [ -n "${image}" ] ; then
        result="${result}image [${image_h}x${image_w}] ${image}${NL}${NL}"
    fi;
    result="${result}tags:[${tags/, /}]${NL}"
    result="${result}============================${NL}"
    :;
    
    echo "${result}"
}

fetchRants() {
    local url=${1:?URL missing}
    local json=$(curl -kL -s "${url}");
    local path="rant";
    clearParsed;
    parseJson "${json}";

    local result=$(BJMODE=find parseJson "success");

    if [ "${result}" != "true" ] ; then
        echo "Could not retrieve rants from url: ${url}" >&2;
        return;
    fi;

    result=$(BJMODE=find parseJson "rant");
    if [ -z "${result}" ] ; then
        result=$(BJMODE=find parseJson "rants");
        path="rants";
    fi;

    if [ -z "${result}" ] ; then
        echo "Failed to determine response type" >&2;
        return;
    fi;

    if [ "${path}" = "rant" ] ; then
        buildRantText "${path}";
    elif [ "${path}" = "rants" ] ; then
        for idx in ${result//,/} ; do
            buildRantText "${path}[${idx}]";
            :;
        done;
        :;
    fi;


}




startServer() {
    local name=${1:?Server name is missing}
    local url=${2:?Data providing url missing}
    local retention=${3:-60}
    
    
    if [ ! -p /dev/rant/${name} ] ; then
        mkfifo /dev/rant/${name};
        chmod 644 /dev/rant/${name};
        :;
    fi;
    
    result=$(fetchRants "${url}")
    
    LAST_FETCH=${SECONDS}
    
    while :; do 
        if [[ $((SECONDS - LAST_FETCH)) -gt ${retention} ]] ; then
            #eval "result=\$(${dataFn})"
            result=$(fetchRants "${url}")
            LAST_FETCH=${SECONDS};
        fi;
        
        echo "${result}" > /dev/rant/${name};
        
        ## cat or any other reader from pipe might be much slower than this loop.
        sleep .3
        :;
    done;
    
}




(startServer feed "${URL_DAY_FEED}")&     PIDS="${PIDS} ${!}"
(startServer algo "${URL_DAY_ALGO}")&     PIDS="${PIDS} ${!}"
(startServer surprise "${URL_SURPRISE}")& PIDS="${PIDS} ${!}"
(startServer stories "${URL_STORY}")&     PIDS="${PIDS} ${!}"


echo "PIDS: ${PIDS}"

