# /dev/rant/

### A simple devrant.com integration into Linux via /dev/rant


#### Example:

    sudo ./sdev_daemon.sh 

    PIDS:  27632 27633 27634 27636

 NOTE: daemon is required to update /dev/rant/<topic> files. pidfiles, stop, restart commands are not implemented yet. Use `sudo kill` to terminate the daemon. Running multiple daemons simultaneously will result in undefined behaviour.

   `cat /dev/rant/surprise`
